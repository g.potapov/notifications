module.exports = {
    apps: [
        {
            watch: ['app'],
            name: 'notifications',
            script: 'app/bin/server.js',
        },

        {
            watch: ['app'],
            name: 'worker',
            script: 'app/bin/worker.js',
        }
    ]
};
