DROP TABLE IF EXISTS notifications_requests CASCADE;
DROP TABLE IF EXISTS notifications_type CASCADE;
DROP TABLE IF EXISTS notifications_channel CASCADE;
DROP TABLE IF EXISTS notifications_recipient CASCADE;
DROP TABLE IF EXISTS notifications_people CASCADE;
