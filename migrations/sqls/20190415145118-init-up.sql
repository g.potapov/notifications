CREATE TABLE IF NOT EXISTS notifications_requests(
            id serial PRIMARY KEY,
            status smallint NOT NULL DEFAULT 1,
            params text NOT NULL,
            err_message text
)WITH (
  OIDS=FALSE
);
ALTER TABLE notifications_requests
  OWNER TO notifications;


CREATE TABLE IF NOT EXISTS notifications_channel
(
  id serial PRIMARY KEY,
  name character varying(50) NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE notifications_channel
  OWNER TO notifications;


INSERT INTO notifications_channel(id, name) VALUES(1, 'sms'), (2, 'email');


CREATE TABLE IF NOT EXISTS notifications_people
(
  id serial PRIMARY KEY,
  surname character varying(50) NOT NULL,
  firstname character varying(50) NOT NULL,
  patronymic character varying(50)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE notifications_people
  OWNER TO notifications;


CREATE TABLE notifications_type
(
  id serial PRIMARY KEY,
  name character varying(50) NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE notifications_type
  OWNER TO notifications;

INSERT INTO notifications_type(name) VALUES('alcho'), ('illness'), ('pressure');  

CREATE TABLE IF NOT EXISTS notifications_recipient
(
  id serial PRIMARY KEY,
  channel_id smallint NOT NULL REFERENCES notifications_channel(id) ON DELETE RESTRICT,
  people_id integer NOT NULL REFERENCES notifications_people(id) ON DELETE RESTRICT,
  type_id smallint NOT NULL REFERENCES notifications_type(id) ON DELETE RESTRICT,
  date_begin timestamp without time zone NOT NULL DEFAULT now(),
  date_end timestamp without time zone,
  adress character varying(250) NOT NULL,
  hostname character varying(250),
  org_id integer
)
WITH (
  OIDS=FALSE
);
ALTER TABLE notifications_recipient
  OWNER TO notifications;