const daoNotif = require('../../db/dao')(process.env.NOTIFICATIONS_DB);
const daoMedist = require('../../db/dao')(process.env.MEDIST_DB);

const constants = require('../../utils/constants');

function getDriverNameAndHostnameAndOrgIdByMedResultIds(medResultIdArray) {

    const queryText = `SELECT dd.surname||' '||dd.name||' '||dd.patronymic as driver_name,
                            dd.number as number,
                           dmd.hostname as hostname, 
                           dd.org_id as org_id
                        FROM drivers_medresult dmr
                          JOIN drivers_meddata dmd ON dmr."medData_id"=dmd.id
                          JOIN drivers_driver dd ON dd.id=dmd.driver_id
                        WHERE dmr.id IN (${medResultIdArray.join(',')})
                        ORDER BY dmr.id LIMIT 1`;

    return daoMedist.many(queryText);

}

function getRecipientsInfoByHostnameOrOrgId(hostname, org_id, type) {

    const query = {
        values: [hostname, org_id, type],
        text: `SELECT nc.name as channel, 
                        nr.adress as adress
                FROM notifications_recipient nr
                    JOIN notifications_channel nc ON nc.id=nr.channel_id
                    JOIN notifications_type nt ON nt.id=nr.type_id
                WHERE ((nr.org_id=$2 AND nr.hostname IS NULL)
                        OR (nr.hostname=$1 AND nr.org_id IS NULL) 
                        OR (nr.hostname=$1 AND nr.org_id=$2))
                        AND (now() BETWEEN nr.date_begin AND nr.date_end OR nr.date_end IS NULL)
                        AND nt.name = $3;`
    };

    return daoNotif.many(query);
}

function getNextNotificationQueryFromBd() {

    const query = {
        values: [2, constants.QUERY_SIZE],
        text: `UPDATE notifications_requests SET status=$1 WHERE id IN 
                    (SELECT id FROM notifications_requests WHERE status=1 LIMIT($2)) RETURNING *`
    }

    return daoNotif.many(query);
}

function updateNotificationStatus(result) {

    const { id, status, errMessage } = result;

    const query = {
        values: [id, status, errMessage],
        text: `UPDATE notifications_requests SET status=$2, err_message=$3 WHERE id=$1;`
    }

    return daoNotif.one(query);
}

function getMedresultById(id) {

    const query = {
        values: [id],
        text: `SELECT * FROM drivers_medresult WHERE id=$1;`
    }

    return daoMedist.one(query);
}

function getFilegeneratorParametersFromBd(medResultIdArray) {

    const query = {
        values: [medResultIdArray],
        text: `SELECT dd.surname||' '||dd."name"||' '||dd.patronymic as full_name,
                        dmd.id as meddata_id,
                        dd."year" as birth_day,
                        dorg.name as org_name,
                        TO_CHAR(dmr."date", 'YYYY-MM-DD HH24:MI:SS') as result_date,
                        TO_CHAR(dmd."date", 'YYYY-MM-DD HH24:MI:SS') as views_date,
                        dmd."pressureUpper" as pressure_upper,
                        dmd."pressureLower" as pressure_lower,
                        dmd.alcho as alcho,
                        dmd.temperature as temperature,
                        dmd.complaints as complaints,
                        dmd.pulse as pulse,
                        dmd.beforeafter as inspection_type,
                        dmr.eds as eds,
                        dmr.comment as comment,
                        dm.surname||' '||dm.name||' '||dm.patronymic as medic_name
                    FROM drivers_driver dd
                        JOIN drivers_organization dorg ON dorg.id=dd.org_id
                        JOIN drivers_meddata dmd ON dmd.driver_id=dd.id
                        JOIN drivers_medresult dmr ON dmr."medData_id"=dmd.id
                        JOIN drivers_medics dm ON dm.id=dmr."medUser_id"
                WHERE dmr.id = ANY ($1)
                ORDER BY dmr.id;`
    }

    return daoMedist.many(query);

}

function addNotificationRequest(params) {
    const query = {
        values: [params],
        text: `INSERT INTO notifications_requests(params) VALUES($1);`
    }

    return daoNotif.one(query);
}

module.exports = {
    getDriverNameAndHostnameAndOrgIdByMedResultIds,
    getRecipientsInfoByHostnameOrOrgId,
    getNextNotificationQueryFromBd,
    getFilegeneratorParametersFromBd,
    updateNotificationStatus,
    addNotificationRequest,
    getMedresultById
}