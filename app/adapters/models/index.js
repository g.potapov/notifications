const moment = require('moment');

const constants = require('../../utils/constants');
const sql = require('./sql');

async function loadRecipientsInfo(hostnamesAndOrgids, type) {

    let unionType = type; //для объединения типов нотификации алко1 и алко2 в алко, для совместимости по типу нотификации из базы

    if (type === constants.NOTIFICATION_TYPES.ALKO_1 || type === constants.NOTIFICATION_TYPES.ALKO_2)
        unionType = constants.NOTIFICATION_TYPES.ALKO;

    const tasks = hostnamesAndOrgids.map(({ hostname, org_id }) => sql.getRecipientsInfoByHostnameOrOrgId(hostname, org_id, unionType));

    const results = await Promise.all(tasks);

    const emails = [], sms = [];

    results.forEach(result => {
        result.forEach(({ channel, adress }) => {

            if (channel === constants.MAILING_TYPES.EMAIL)
                emails.push(adress);

            if (channel === constants.MAILING_TYPES.SMS)
                sms.push(adress);
        });
    });

    const hostname = hostnamesAndOrgids[hostnamesAndOrgids.length - 1].hostname;

    return {
        hostname,
        emails,
        sms
    };
}

function loadMedResult(id) {
    return sql.getMedresultById(id);
}

async function loadDriverNameAndHostnamesAndOrgIds(medresult_ids) {
    const resultSet = await sql.getDriverNameAndHostnameAndOrgIdByMedResultIds(medresult_ids);

    const hostnamesAndOrgIds = resultSet.map(({ hostname, org_id }) => ({ hostname, org_id }));
    const driverName = resultSet.reverse()[0].driver_name;
    const number = resultSet.reverse()[0].number

    return {
        driverName,
        number,
        hostnamesAndOrgIds
    }
}

async function loadNotificationsRequests() {

    let requests = null;
    try {
        requests = await sql.getNextNotificationQueryFromBd();

        requests.forEach(result => {
            result.params = JSON.parse(result.params);
        });
    }
    catch (err) {
        console.log('error througth load notifications requests, reason - ', err);
    }

    return requests;
}

async function loadFileGeneratorParameters(medresult_ids) {
    const resultSet = await sql.getFilegeneratorParametersFromBd(medresult_ids);

    const month_declension = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];

    const inspectionTypes = {
        before: "предрейсовый",
        after: "послерейсовый",
        line: "линейный",
        beforeshift: "предсменный",
        aftershift: "послесменный",
        alcho: "контроль трезвости"
    }

    const inspections = resultSet.map(row => {
        let [date, time] = row.views_date.split(' ');

        const [year, month, day] = date.split('-');

        date = `"${day}" ${month_declension[+month]} ${year} г. ${time}`;

        const [hours, minutes, seconds] = time.split(':');
        const dateDetailed = `"${day}" ${month_declension[+month]} ${year} г. в ${hours} час. ${minutes} мин.`;

        const pressureUpper = row.pressure_upper;

        const pressureLower = row.pressure_lower;
        const meddata_id = row.meddata_id;
        const temperature = row.temperature;
        const complaints = row.complaints;
        const pulse = row.pulse;
        const alchohol = row.alcho;
        const eds = row.eds;
        const comment = row.comment;
        const medUser = row.medic_name;
        const inspectionType = inspectionTypes[row.inspection_type];

        return {
            id: `${meddata_id}`,
            date: date,
            dateDetailed: dateDetailed,
            pressureUpper: `${pressureUpper}`,
            pressureLower: `${pressureLower}`,
            temperature: `${temperature}`,
            complaints: complaints ? "Присутствуют" : "Отсутствуют",
            pulse: `${pulse}`,
            alchohol: `${alchohol}`,
            eds: eds,
            comment: comment,
            medUser: medUser,
            inspectionType: inspectionType
        };
    });

    const driver = resultSet[0].full_name;
    let birthday = resultSet[0].birth_day;

    const [bday, bmonth, byear] = birthday.split('-');

    birthday = `${byear}-${bmonth}-${bday}`;


    const getYears = (birthday) => {

        let years = moment().diff(birthday, 'years');

        if (moment() < moment(birthday))
            years -= 1;

        let yearsStr;

        if (years % 10 === 1)
            yearsStr = `${years} год`;
        else if (years % 10 >= 2 && years % 10 <= 4)
            yearsStr = `${years} года`;
        else
            yearsStr = `${years} лет`;

        return yearsStr;
    }

    const orgName = resultSet[0].org_name;

    const [date, time] = resultSet[0].result_date.split(' ');
    const id = medresult_ids[medresult_ids.length - 1];

    const getTime = (time) => {
        const [hours, minutes, seconds] = time.split(':');

        return `${hours}:${minutes}`;
    }

    return {
        printData: {
            id: `${id}`,
            driver: driver,
            birthday: birthday,
            years: getYears(birthday),
            device: "Алкотектор Юпитер",
            orgName: orgName,
            date: `${date}`,
            time: getTime(time),
            inspections
        },
    };
}

function updateNotificationStatus(result) {
    return sql.updateNotificationStatus(result);
}

function addNotificationRequest(params) {
    return sql.addNotificationRequest(JSON.stringify(params));
}

module.exports = {
    loadNotificationsRequests,
    loadDriverNameAndHostnamesAndOrgIds,
    loadRecipientsInfo,
    loadFileGeneratorParameters,
    updateNotificationStatus,
    addNotificationRequest,
    loadMedResult
}