module.exports = function (app, express) {

    const createError = require('http-errors');
    const cookieParser = require('cookie-parser');

    const setRoutes = require('../../routes');

    app.set('views', '/app/views');
    app.set('view engine', 'pug');

    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(express.static('/app/public'));

    setRoutes(app);

    app.use(function (req, res, next) {
        next(createError(404));
    });

    app.use(function (err, req, res, next) {
        console.log(`bad request, reason - ${err.message}, stack - `, err.stack);

        res.status(err.status || 500).send();
    });
}