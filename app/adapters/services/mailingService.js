const caller = require('../../utils/serviceCaller');

const constants = require('../../utils/constants');

module.exports = async function callMailingService(data, mailingType) {
    let mailingData;

    if (mailingType === constants.MAILING_TYPES.EMAIL) {
        mailingData = {
            channel: mailingType,
            responders: data.emails,
            body: {
                title: data.emailMessage.title,
                message: data.emailMessage.message,
                stampId: data.emailMessage.stampId,
                subject: data.emailMessage.subject
            },
            attachmentsUrl: data.attachmentsUrl
        };
    }

    if (mailingType === constants.MAILING_TYPES.SMS) {
        mailingData = {
            channel: mailingType,
            responders: data.sms,
            body: { text: data.smsMessage }
        };
    }

    try {
        console.log('mailing type - ', mailingType);
        console.log('mailing data - ', mailingData);

        await caller({
            method: 'POST',
            uri: process.env.URL_MAILING_SERVICE,
            body: mailingData
        });
    }
    catch (err) {
        throw new Error(`error througth calling mailing service, message: ${err.message}`);
    }
}