const caller = require('../../utils/serviceCaller');

const constants = require('../../utils/constants');

const _getModuleNameAndFolder = (type) => {

    let moduleName, folder;

    switch (type) {
        case constants.NOTIFICATION_TYPES.ALKO_1:
        case constants.NOTIFICATION_TYPES.ALKO_2:
            moduleName = 'pp-module-filegen-alcho';
            folder = 'notifications_alcho';
            break;
        case constants.NOTIFICATION_TYPES.PRESSURE:
            moduleName = 'pp-module-filegen-reference_several';
            folder = 'notifications_pressure';
            break;
        case constants.NOTIFICATION_TYPES.ILLNESS:
            moduleName = 'pp-module-filegen-reference_single';
            folder = 'notifications_illness';
            break;
    }

    return {
        moduleName,
        folder
    };
}

module.exports = async function callFileGeneratorService(type, params, reason) {

    const { moduleName, folder } = _getModuleNameAndFolder(type);

    const data = {
        ...params,
        moduleName,
        folder,
        action: 'save'
    }

    data.printData.inspections.forEach(inspection => {
        inspection.reason = reason;
    });

    try {
        const response = await caller({
            method: 'POST',
            uri: process.env.URL_FILE_GEN_SERVICE,
            body: data
        });

        const subPath = response.path.substr(response.path.indexOf('static'));

        return [`${process.env.STATIC_URL}/${subPath}`];
    }
    catch (err) {
        throw new Error(`error througth calling file generator service, message: ${err.message}`);
    }
}