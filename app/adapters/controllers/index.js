const notificationModel = require('../models');
const constants = require('../../utils/constants');

const createError = require('http-errors');

async function validateInputParams(params) {
    const { medresult_ids, type, reason } = params;

    const validateMesssage = [];

    if (!medresult_ids)
        validateMesssage.push('medresult_ids required!');

    if (!type)
        validateMesssage.push('type required!');

    if (!reason)
        validateMesssage.push('reason required!');

    if (!Array.isArray(medresult_ids)) {
        validateMesssage.push('Medresult_ids incorrenct type, need array of numbers.');
    }
    else {
        if (!medresult_ids.length)
            validateMesssage.push(`empty medresult_ids array.`);

        if (medresult_ids.some(id => !Number.isInteger(id))) {
            validateMesssage.push(`One or more medresults_ids has incorrenct type, need integer number.`);
        }
        else {
            await Promise.all(
                medresult_ids.map(async m_id => {
                    const { id } = await notificationModel.loadMedResult(m_id);

                    if (!id)
                        validateMesssage.push(`Medresult id ${id} doesnt exist`);
                })
            )
        }
    }

    if (!Object.values(constants.NOTIFICATION_TYPES).includes(type))
        validateMesssage.push(`${type} is unknown notification type`);

    if (validateMesssage.length) {

        throw createError(400, `validate params error - ${validateMesssage.join(', ')}`);
    }
}

async function incomingData(req, res, next) {
    try {
        console.log(`Incoming request`, req.body);

        validateInputParams(req.body);

        await notificationModel.addNotificationRequest(req.body);

        res.status(200).send();
    }
    catch (err) {
        next(err);
    }
}

module.exports = {
    incomingData,
}