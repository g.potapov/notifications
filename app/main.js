
const express = require('express');
const setMiddlewares = require('./adapters/middlewares');

const app = express();
setMiddlewares(app, express);

module.exports = app;
