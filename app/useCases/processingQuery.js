const notificationModel = require('../adapters/models');

const { createNotificationFactory } = require('../entities');

const callFileGeneratorService = require('../adapters/services/fileGenService');
const callMailingService = require('../adapters/services/mailingService');

const constants = require('../utils/constants');

//TODO add logging

async function processQuery() {
    const notifRequests = await notificationModel.loadNotificationsRequests();

    if (!notifRequests || !notifRequests.length )
        return;

    const sendedTasks = notifRequests.map(async ({ id, params }) => {

        const result = {
            id,
            status: constants.WORKER_STATUSES.SUCCESS,
            errMessage: 'no error'
        }

        try {
            await _processNotification(params);
        }
        catch (err) {
            console.log(`notification id - ${id}, error: `, err.message);

            result.errMessage = err.message;
            result.status = constants.WORKER_STATUSES.FAIL;
        }

        notificationModel.updateNotificationStatus(result);
    });

    await Promise.all(sendedTasks);
}

async function _processNotification(params) {

    const notification = await _createNotification(params);

    if (!notification)
        return;

    await _sendNotification(notification);
}

async function _createNotification(params) {
    const { type, medresult_ids, reason } = params;

    try {

        const { driverName, hostnamesAndOrgIds, number } = await notificationModel.loadDriverNameAndHostnamesAndOrgIds(medresult_ids);

        console.log(`driver name - ${driverName}, number - ${number}, hostnamesAndOrgIds - `, hostnamesAndOrgIds);

        const { hostname, emails, sms } = await notificationModel.loadRecipientsInfo(hostnamesAndOrgIds, type);

        if (sms.length === 0 && emails.length === 0)
            return;

        console.log(`hostname - ${hostname}, emails - ${emails}, sms - ${sms}`);

        let attachmentsUrl = null;

        if (type !== constants.NOTIFICATION_TYPES.ALKO_1) {
            const params = await notificationModel.loadFileGeneratorParameters(medresult_ids);

            console.log('file gen params - ', params);

            console.log(process.env.URL_FILE_GEN_SERVICE);

            attachmentsUrl = await callFileGeneratorService(type, params, reason);

            console.log('attachments urls - ', attachmentsUrl);
        }

        const constructionParams = {
            reason,
            hostname,
            emails,
            sms,
            driverName,
            number,
            attachmentsUrl,
        };

        return createNotificationFactory(type, constructionParams);
    }
    catch (err) {
        console.log('error througth create notification, stack - ', err.stack);
        throw new Error(`error througth create notification, reason - ${err.message}`);
    }
}

async function _sendNotification(notification) {
    try {

        const { emails, sms } = notification.getAdresses();

        const emailData = {
            emails: emails,
            emailMessage: notification.getEmailMessage(),
            attachmentsUrl: notification.getAttachmentsUrls()
        }

        const smsData = {
            sms: sms,
            smsMessage: notification.getSmsMessage()
        }

        const callingTasks = [];

        if (emailData.emails.length > 0)
            callingTasks.push(callMailingService(emailData, constants.MAILING_TYPES.EMAIL));

        if (smsData.sms.length > 0)
            callingTasks.push(callMailingService(smsData, constants.MAILING_TYPES.SMS));

        if (callingTasks.length > 0)
            await Promise.all(callingTasks);
    }
    catch (err) {
        console.log('error througth sending notification, stack - ', err.stack);
        throw new Error(`error througth sending notification, reason - ${err.message}`)
    }
}

module.exports = {
    processQuery,
}