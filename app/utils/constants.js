module.exports = {
    NOTIFICATION_TYPES: {
        ALKO: 'alcho',
        ALKO_1: 'alcho1',
        ALKO_2: 'alcho2',
        ILLNESS: 'illness',
        PRESSURE: 'pressure'
    },

    MAILING_TYPES: {
        SMS: 'sms',
        EMAIL: 'email'
    },

    WORKER_STATUSES: {
        FREE: 1,
        BUSY: 2,
        SUCCESS: 3,
        FAIL: 4
    },

    QUERY_SIZE: 50
}

