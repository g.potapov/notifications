const rp = require('request-promise');

module.exports = (params) => {

    const options = {
        ...params,
        json: true,
        encoding: null
    };

    return rp(options);
}