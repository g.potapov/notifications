const constants = require('../utils/constants');

class INotification {
    constructor(params) {

        this.driverName = params.driverName;
        this.number = params.number;
        this.hostname = params.hostname;
        this.reason = params.reason;

        this.emails = params.emails;
        this.sms = params.sms

        this.attachmentsUrl = params.attachmentsUrl;
    }

    getDriverShortName() {
        const [lastname, firstname, patronymic] = this.driverName.split(' ');
        const shortname = `${lastname} ${firstname.charAt(0)}.${patronymic.charAt(0)}`;
        return shortname;
    }

    getAdresses() {
        return {
            emails: this.emails,
            sms: this.sms
        };
    }

    getAttachmentsUrls() {
        return this.attachmentsUrl;
    }

    getEmailMessage() {
        throw new Error('method not implemented!');
    }

    getSmsMessage() {
        throw new Error('method not implemented!');
    }
}

class Alco_1_Notification extends INotification {

    static create(params) {
        return new Alco_1_Notification(params);
    }

    getEmailMessage() {
        const message = `Здравствуйте! ${this.getDriverShortName()}, табельный номер: ${this.number} на терминале ${this.hostname} не прошел первичное алкотестирование. Необходимо провести повторный медосмотр через 20 минут. С Уважением, Medpoint24."`;
        const subject = "Медпоинт24: Обнаружение паров этанола в выдыхаемом воздухе у сотрудника.";
        const title = subject;
        const stampId = "test";

        return {
            subject,
            message,
            title,
            stampId
        };
    }

    getSmsMessage() {
        const message = `${this.getDriverShortName()}, табельный номер: ${this.number} на терминале ${this.hostname} не прошел первичное алкотестирование. Необходимо провести повторный медосмотр через 20 минут.`;
        return message;
    }
}

class Alco_2_Notification extends INotification {

    static create(params) {
        return new Alco_2_Notification(params);
    }

    getEmailMessage() {
        const message = `Здравствуйте! ${this.getDriverShortName()}, табельный номер: ${this.number} на терминале ${this.hostname} не прошел повторное алкотестирование. Сформированная документация во вложении. Срок действия документации не более 2 часов с даты формирования. С Уважением, Medpoint24.`;
        const subject = "Медпоинт24: Обнаружение паров этанола в выдыхаемом воздухе у сотрудника.";
        const title = subject;
        const stampId = "test";

        return {
            subject,
            message,
            title,
            stampId
        };
    }

    getSmsMessage() {
        const message = `${this.getDriverShortName()}, табельный номер: ${this.number} на терминале ${this.hostname} не прошел повторное алкотестирование. Сформированная документация направлена на e-mail ответственному.`;
        return message;
    }
}

class Illness_Notification extends INotification {

    static create(params) {
        return new Illness_Notification(params);
    }

    getEmailMessage() {
        const message = `Здравствуйте! ${this.getDriverShortName()}, табельный номер: ${this.number} на терминале ${this.hostname} не прошел медосмотр по причине: '${this.reason}'. Сформированная документация во вложении. Срок действия документации не более 24 часов с даты формирования.`;
        const subject = "Медпоинт24: Направление к врачу.";;
        const title = subject;
        const stampId = "test";

        return {
            message,
            subject,
            title,
            stampId
        };
    }

    getSmsMessage() {
        const message = `${this.getDriverShortName()}, табельный номер: ${this.number} на терминале ${this.hostname} не прошел медосмотр по причине: '${this.reason}''.  Сформированная документация направлена на e-mail ответственному`;
        return message;
    }
}

class Pressure_Notification extends INotification {

    static create(params) {
        return new Pressure_Notification(params);
    }

    getEmailMessage() {
        const message = `Здравствуйте! ${this.getDriverShortName()}, табельный номер: ${this.number} на терминале ${this.hostname} не прошел медосмотр по причине: '${this.reason}'. Сформированная документация во вложении. Срок действия документации не более 24 часов с даты формирования.`;
        const subject = "Медпоинт24: Направление к врачу.";
        const title = subject;
        const stampId = "test";

        return {
            message,
            subject,
            title,
            stampId
        };
    }

    getSmsMessage() {
        const message = `${this.getDriverShortName()}, табельный номер: ${this.number} на терминале ${this.hostname} не прошел медосмотр по причине: '${this.reason}''.  Сформированная документация направлена на e-mail ответственному`;
        return message;
    }
}

function createNotificationFactory(type, constructionParams) {

    let notification;

    if (type === constants.NOTIFICATION_TYPES.ALKO_1) {
        notification = Alco_1_Notification.create(constructionParams);
    }

    if (type === constants.NOTIFICATION_TYPES.ALKO_2) {
        notification = Alco_2_Notification.create(constructionParams);
    }

    if (type === constants.NOTIFICATION_TYPES.PRESSURE) {
        notification = Pressure_Notification.create(constructionParams);
    }

    if (type === constants.NOTIFICATION_TYPES.ILLNESS) {
        notification = Illness_Notification.create(constructionParams);
    }

    if (!notification)
        throw new Error(`could not create notification entity with type - ${type}!`);

    return notification;
}

module.exports = {
    createNotificationFactory,
}