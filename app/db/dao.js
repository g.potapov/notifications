const { Pool } = require('pg');

module.exports = function (connectionString) {

    const pool = new Pool({
        connectionString: connectionString
    });

    pool.on('error', (err, client) => {
        console.log('Unexpected error on idle client', err);
    });

    const _query = async (querySql) => {

        try {
            const result = await pool.query(querySql);
            return result;
        }
        catch (err) {
            console.log('bad sql stack - ', err.stack);
            throw new Error(`bad sql, message - ${err.message}, text - ${querySql.text}, params - ${querySql.values}`);
        }
    }

    const one = (querySql) => {
        return _query(querySql)
            .then(result => result.rows[0] ? result.rows[0] : {});
    }

    const many = (querySql) => {
        return _query(querySql)
            .then(result => result.rows);
    }

    return {
        one,
        many
    }
}
