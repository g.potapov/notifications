const incomingDataController = require('./adapters/controllers');

module.exports = (app) => {
    app.post('/notifications', incomingDataController.incomingData);
}