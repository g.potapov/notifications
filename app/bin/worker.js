const dotenv = require('dotenv');
dotenv.config();

const logic = require('../useCases/processingQuery');

interval(logic.processQuery);

function interval(cb, delay = 5 * 1000) {
    setTimeout(async function task() {
        try {
            await cb();
            setTimeout(task, delay);
        }
        catch (err) {
            console.log(`unexpected error - ${err.message}, stack - ${err.stack}`);
        }
    }, delay);
}
