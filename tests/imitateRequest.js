const request = require('request');

async function getrequestOptions() {

    const testHost = process.argv[2] || '127.0.0.1:3088';

    const reqData = {
        type: "alcho2",
        medresult_ids: [4762302, 4762303, 4762304],
        reason: "test reason"
    };

    return {
        method: 'post',
        body: reqData,
        json: true,
        url: `http://${testHost}/notifications`
    };
}
/*
10.10.10.234:3088
*/

function imitateRequest(options) {

    return new Promise((resolve, reject) => {
        request(options, function (err, res, body) {
            if (err)
                console.log(err.message);

            resolve();
        });
    });
}

async function cycle() {

    const requestOptions = await getrequestOptions();

    for (let i = 0; i < 2; i++) {
        console.log(i, requestOptions);

        await imitateRequest(requestOptions);

        console.log('finish', i);
    }
}

cycle();
