#!/bin/bash

exec node /usr/local/lib/node_modules/db-migrate/bin/db-migrate $@ --config $(pwd)/config/database.json --sql-file --env pg

